#pragma once
#include <vector>

template <typename T>
class Utils
{
public:
	struct Comparator
	{
		bool operator()(const Utils & left, const Utils & right) const
		{
			return left.mv_Value < right.mv_Value;
		}
	};

	Utils() = default;
	Utils(T argVal) :mv_Value(argVal) {}

	auto mf_MaxElement(T lv_FirstElement, T lv_SecondElement) const -> T
	{
		return lv_FirstElement > lv_SecondElement ? lv_FirstElement : lv_SecondElement;
	}

	T elementMean()const
	{
		int half = valVector.size() / 2;
		
		return valVector[half];
	}

	void setVector(const std::vector<T>& vector)
	{
		valVector = vector;
	}

	~Utils() = default;

private:
	std::vector<T> valVector;
	T mv_Value;
};
